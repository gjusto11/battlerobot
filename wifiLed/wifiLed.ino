#include <ESP8266WiFi.h>
// DOCUMENTACIÓN: https://roboticadiy.com/esp8266-make-your-own-led-control-web-server-in-arduino-ide/
// Introduce el nombre de tu red wifi y la contraseña Wifi
const char* ssid = "ECAlmagro";
const char* password = "a3delateneo";

// Establezca el número de puerto del servidor web en 80
WiFiServer server(80);

// Variable para almacenar la petición HTTP
String header;

// Estas variables almacenan el estado actual de la salida del LED
String outputRedState = "off";
String outputGreenState = "off";
String outputBlueState = "off";

// Asignar variables de salida a los pines GPIO
const int redLED = D1;
const int greenLED = D4;
const int blueLED = 2;

// Hora actual
unsigned long currentTime = millis();
// Tiempo anterior
unsigned long previousTime = 0;
// Definir el tiempo de espera en milisegundos (ejemplo: 2000ms = 2s)
const long timeoutTime = 2000;

void setup() {
  Serial.begin(115200);
  // Inicializar las variables de salida como salidas
  pinMode(redLED, OUTPUT);
  pinMode(greenLED, OUTPUT);
  pinMode(blueLED, OUTPUT);
  // Set outputs to LOW
  digitalWrite(redLED, LOW);
  digitalWrite(greenLED, LOW);
  digitalWrite(blueLED, HIGH);

  // Conéctate a la red Wi-Fi con el SSID y la contraseña
  Serial.print("Conectar con ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Imprimir la dirección IP local e iniciar el servidor web
  Serial.println("");
  Serial.println("WiFi conectado.");
  Serial.println("IP: ");
  Serial.println(WiFi.localIP());
  server.begin();
}

void loop() {
  WiFiClient client = server.available(); // Escuchar a los clientes que llegan

  if (client) { // Si se conecta un nuevo cliente,
    Serial.println("Conectado."); // imprimir un mensaje en el puerto serie
    String currentLine = ""; // hacer un String para contener los datos entrantes del cliente
    currentTime = millis();
    previousTime = currentTime;
    while (client.connected() && currentTime - previousTime <= timeoutTime) { // bucle mientras el cliente está conectado
      currentTime = millis();
      if (client.available()) { // si hay bytes que leer del cliente,
        char c = client.read(); // leer un byte, entonces
        Serial.write(c); // imprimirlo en el monitor de serie
        header += c;
        if (c == '\n') { // si el byte es un carácter de nueva línea
        // si la línea actual está en blanco, tienes dos caracteres de nueva línea seguidos.
        // es el fin de la petición HTTP del cliente, así que envía una respuesta:
          if (currentLine.length() == 0) {
            // Las cabeceras HTTP siempre comienzan con un código de respuesta (por ejemplo, HTTP/1.1 200 OK)
            // y un tipo de contenido para que el cliente sepa lo que viene, y luego una línea en blanco:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();

            // enciende y apaga los GPIOs
            if (header.indexOf("GET /2/on") >= 0) {
              Serial.println("RED LED esta encendido");
              outputRedState = "on";
              digitalWrite(redLED, HIGH);
            }
            else if (header.indexOf("GET /2/off") >= 0) {
              Serial.println("RED LED esta apagado");
              outputRedState = "off";
              digitalWrite(redLED, LOW);
            }
            else if (header.indexOf("GET /4/on") >= 0) {
              Serial.println("Green LED is on");
              outputGreenState = "on";
              digitalWrite(greenLED, HIGH);
            }
            else if (header.indexOf("GET /4/off") >= 0) {
              Serial.println("Green LED esta apagado");
              outputGreenState = "off";
              digitalWrite(greenLED, LOW);
            }
            else if (header.indexOf("GET /5/on") >= 0) {
              Serial.println("Blue LED is on");
              outputBlueState = "on";
              digitalWrite(blueLED, LOW);
            }
            else if (header.indexOf("GET /5/off") >= 0) {
              Serial.println("Blue LED esta apagado");
              outputBlueState = "off";
              digitalWrite(blueLED, HIGH);
            }

            // Mostrar la página web HTML
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            // CSS para estilizar los botones de encendido/apagado
            client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
            client.println("body { background-color: black; color: white;}");
            client.println(".buttonRed { background-color: red; border: none; color: white; heigth: 200px; width 200px; padding: 30px; border-radius: 100%;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            client.println(".buttonGreen { background-color: green; border: none; color: white; heigth: 200px; width 200px; padding: 30px; border-radius: 100%;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            client.println(".buttonBlue { background-color: blue; border: none; color: white; heigth: 200px; width 200px; padding: 30px; border-radius: 100%;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            client.println(".buttonOff { background-color: #77878A; border: none; color: white; heigth: 200px; width 200px; padding: 30px; border-radius: 100%;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}</style></head>");

            // Título de la página web
            client.println("<body><h1>El Ateneo Server</h1>");

            // Indicación del estado actual y botones ON/OFF para GPIO 2 LED rojo
            client.println("<p>LED rojo esta " + outputRedState + "</p>");
            // Si la salidaRedState está apagada, muestra el botón OFF
            if (outputRedState == "off") {
              client.println("<p><a href=\"/2/on\"><button class=\"button buttonOff\">OFF</button></a></p>");
            }
            else {
              client.println("<p><a href=\"/2/off\"><button class=\"button buttonRed\">ON</button></a></p>");
            }

            // Muestra el estado actual, y botones ON/OFF para GPIO 4 LED verde
            client.println("<p>LED verde esta " + outputGreenState + "</p>");
            // Si el outputGreenState está apagado, muestra el botón OFF
            if (outputGreenState == "off") {
              client.println("<p><a href=\"/4/on\"><button class=\"button buttonOff\">OFF</button></a></p>");
            }
            else {
              client.println("<p><a href=\"/4/off\"><button class=\"button buttonGreen\">ON</button></a></p>");
            }
            client.println("</body></html>");

            // Indicación del estado actual y botones ON/OFF para GPIO 5 LED azul
            client.println("<p>LED azul esta " + outputBlueState + "</p>");
            // Si la salidaBlueState está apagada, muestra el botón OFF
            if (outputBlueState == "off") {
              client.println("<p><a href=\"/5/on\"><button class=\"button buttonOff\">OFF</button></a></p>");
            }
            else {
              client.println("<p><a href=\"/5/off\"><button class=\"button buttonBlue\">ON</button></a></p>");
            }
            client.println("</body></html>");

            // La respuesta HTTP termina con otra línea en blanco
            client.println();
            // Salir del bucle while
            break;
          }
          else { // si tiene una nueva línea, entonces borre currentLine
            currentLine = "";
          }
        }
        else if (c != '\r') { // si tiene algo más que un carácter de retorno de carro,
          currentLine += c; // añadirlo al final de la línea actual
        }
      }
    }
    // Borrar la variable de cabecera
    header = "";
    // Cerrar la conexión
    client.stop();
    Serial.println("Desconectado");
    Serial.println("");
  }
}
