#include <ESP8266WiFi.h>
// DOCUMENTACIÓN: https://www.luisllamas.es/como-conectar-un-esp8266-a-una-red-wifi-modo-sta/
// Sustituir con datos de nuestra red
const char* ssid     = "ECAlmagro";
const char* password = "a3delateneo";

void setup()
{
  Serial.begin(115200);
  delay(10);
  
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.print("\tConectando a:\t");
  Serial.println(ssid); 

  // Esperar a que nos conectemos
  while (WiFi.status() != WL_CONNECTED) 
  {
    delay(200);
   Serial.print('.');
  }

  // Mostrar mensaje de exito y dirección IP asignada
  Serial.println();
  Serial.print("Conectado a:\t");
  Serial.println(WiFi.SSID()); 
  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP());
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() 
{
  digitalWrite(LED_BUILTIN, LOW);   // Encienda el LED (Tener en cuenta que LOW es el nivel de tensión
  // pero en realidad el LED está encendido; esto es porque es activo bajo en el ESP-01)
  delay(300);
  digitalWrite(LED_BUILTIN, HIGH);  // Apagar el LED haciendo que la tensión sea HIGH
  delay(300);
}
